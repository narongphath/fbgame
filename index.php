<?php
	/* Inlucde header */
	require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/header.php'; 
	$sql = "SELECT * FROM games ORDER BY id DESC";
	/* query all posts(games) */
	$games = items_list($sql, $db);
	$games->setFetchMode(PDO::FETCH_ASSOC);
?>
<div class="container home-container">
	<div class="row">
		<div class="col s12 m8">
				<div class="row">
					<?php while($game = $games->fetch()) : ?>
						<div class="col s12 m6">
							<p class="post-title">
								<a href="game/?gid=<?php echo $game['id']; ?>">
									<?php echo $game['name']; ?>
								</a>
							</p>
							<div class="card">
								<div class="card-image">
									<img src="img/game/<?php echo $game['id']; ?>/game-<?php echo $game['id']; ?>.png" class="response-img">
								</div>
								<div class="card-stacked">
									<div class="card-content">
										<?php
											$excerpt = mb_substr($game['description'], 0, 300) . "...";
										?>
										<p><?php echo $excerpt; ?></p>
									</div>
									<div class="card-action">
										<a href="game/?gid=<?php echo $game['id']; ?>" class="waves-effect waves-light btn blue">
											<i class="material-icons right">trending_flat</i>មើលលំអិត
										</a>
									</div>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
		</div><!-- /  Page content  -->
		
		<!-- Include Sidebar -->
		<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/sidebar.php'; ?>
	</div><!-- /.row -->
</div><!-- /.container -->

<!-- Include Footer -->
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/footer.php'; ?>
