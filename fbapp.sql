-- MySQL dump 10.13  Distrib 5.5.50, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: c9
-- ------------------------------------------------------
-- Server version	5.5.50-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `games`
--

DROP TABLE IF EXISTS `games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `games` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(160) DEFAULT NULL,
  `description` text,
  `created_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `games`
--

LOCK TABLES `games` WRITE;
/*!40000 ALTER TABLE `games` DISABLE KEYS */;
INSERT INTO `games` VALUES (1,'តើឆ្នាំនេះអ្នកមានឈ្មោះ ក្នុងបញ្ជីយមរាជ ហើយឬនៅ?','ហ្គេម តើឆ្នាំនេះអ្នកមានឈ្មោះ ក្នុងបញ្ជីយមរាជ ហើយឬនៅ? ត្រូវបានបង្កើតឡើងក្នុងគោលបំណង ដើម្បីធ្វើ​អោយ​មាន​ភាព​ប្លែក ក្នុងន័យកំសាន្ត មិនមែនប្រមាទ ជីវិត​នរណាម្នាក់ឡើយ។ \nលោកអ្នកអាចលេង​ហ្គេម​នេះបាន​ដោយគ្រាន់តែ​ចុចលើពាក្យ សាកល្បង បន្ទាប់មក វាទាមទារ អោយ​លោកអ្នក​ធ្វើការ​ភ្ជាប់ជាមួយនឹង ហ្វេសប៊ុក​របស់លោកអ្នក ដើម្បីទាញយក​ទិន្នន័យ។ បើសិនជា​ លោកអ្នក​មិន​ពេញចិត្ត​នឹង​លទ្ធផល លោកអ្នកអាច​ចុច​លើ​បូតុង​លេងម្តង​ទៀត។','2016-09-08'),(2,'តើទម្រង់លេងរបស់អ្នកដូចនឹង​ កីឡាករ រូបណា?','នៅក្នុងហ្កេមនេះគឺតម្រូវ អោយអ្នកជ្រើសរើសតួនាទីដែលអ្នកចូលចិត្តលេង ដូចជា៖ អ្នកចាំទី ខ្សែរការពារ ខ្សែរបំរើ ឬ ខ្សែរប្រយុទ្ធជាដើម។​ លោកអ្នកអាចលេង​ហ្គេម​នេះបាន​ដោយគ្រាន់តែ​ចុចលើពាក្យ សាកល្បង បន្ទាប់មក វាទាមទារ អោយ​លោកអ្នក​ធ្វើការ​ភ្ជាប់ជាមួយនឹង ហ្វេសប៊ុក​របស់លោកអ្នក ដើម្បីទាញយក​ទិន្នន័យ។ បើសិនជា​ លោកអ្នក​មិន​ពេញចិត្ត​នឹង​លទ្ធផល លោកអ្នកអាច​ចុច​លើ​បូតុង​លេងម្តង​ទៀត។','2016-09-08');
/*!40000 ALTER TABLE `games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `postion` varchar(40) NOT NULL,
  `player_scope` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES (1,'Lionel Messi','forward','2'),(2,'Cristiano Ronaldo','forward','2'),(3,'Luis Suarez','forward','2'),(4,'Chan Vattanaka','forward','1'),(5,'Prak Monyuthom','forward','1'),(6,'Pepe','defender','2'),(7,'Isco','midfielder','2'),(8,'De Gea','goalkeeper','2'),(9,'Kasillas','goalkeeper','2'),(10,'Ramos','defender','2'),(11,'Toni Kroos','midfielder','2');
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profiles`
--

DROP TABLE IF EXISTS `profiles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `userid` varchar(20) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `urlid` varchar(25) DEFAULT NULL,
  `play` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profiles`
--

LOCK TABLES `profiles` WRITE;
/*!40000 ALTER TABLE `profiles` DISABLE KEYS */;
INSERT INTO `profiles` VALUES (3,'Kimsoeurn Sok','1057760724338078',NULL,'2-1057760724338078-2',2),(4,'Pa Bella','10210320951563447',NULL,'10210320951563447-1',1),(5,'Kevin Chow','1695629440762977',NULL,'2-1695629440762977-139',139),(6,'Narong Madridista','1075585482509761',NULL,'2-1075585482509761-96',96);
/*!40000 ALTER TABLE `profiles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-15  4:33:14
