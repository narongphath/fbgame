<?php
  /* Include header */
  require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/header.php';
?>
<div class="container game-container">
  <div class="row">
    <div class="col s12 m8">
      <!-- Check if there's game id and game id is valid -->
      <?php if(isset($_GET['gid']) && $check_exists !== 0) : ?>
        <?php $game = show_single($sql, $db); ?>
        <div class="card">
          <div class="card-image">
            <img src="<?php echo $site_root; ?>/img/game/<?php echo $_GET['gid']; ?>/game-<?php echo $_GET['gid']; ?>.png">
          </div>
          <div class="card-content">
            <h4><?php echo $game->name; ?></h4>
            <p><?php echo $game->description; ?></p>
          </div>
          <div class="card-action">
          <?php if($_GET['gid'] == 2 ) : ?>
            <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/football-option.php'; ?>
          <?php else: ?>
            <?php echo $login_link; ?>
          <?php endif; ?>
          </div>
        </div>
      <?php else : ?>
        <!-- Include Sidebar -->
        <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/404.php'; ?>
      <?php endif; ?>
    </div><!-- Game Content -->
    
    <!-- Include Sidebar -->
    <?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/sidebar.php'; ?>
  </div>
</div><!--/.game-container -->

<!-- Include Footer -->
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/footer.php'; ?>
