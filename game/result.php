<?php 
  require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/header.php';
?>
 <div class="container">
  <div class="row">
		<div class="col s12 m8">
			<?php if(isset($_GET['pid']) && $check_exists !== 0) : ?>
				<div class="card">
				  <div class="card-image">
					<img src="<?php echo $thumb_img; ?>">
				  </div>
				  <div class="card-action">
					<a href="http://www.facebook.com/sharer.php?u=<?php echo $page_link; ?>" class="fb-share-btn waves-effect waves-light btn blue darken-3">Share</a>
					<?php if (isset($_SESSION['fb_access_token'])) : ?>
					<a href="index.php?gid=<?php echo $gid; ?>" class="waves-effect try-again waves-light btn orange darken-3">ទាយម្តងទៀត</a>
					<?php endif; ?>
				  </div>
				</div>
			<?php else: ?>
				<h2><?php echo "ERROR 404"; ?></h2>
			<?php endif; ?>
			</div>
        <div class="col s12 m4">
        </div>
      </div>
    </div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo $site_root; ?>/js/app.js"></script>
</html>