<?php
require 'vendor/autoload.php';
use Firebase\Token\TokenException;
use Firebase\Token\TokenGenerator;

try {
    $generator = new TokenGenerator('<YOUR_FIREBASE_SECRET>');
    $token = $generator
        ->setData(array('uid' => 'exampleID'))
        ->create();
} catch (TokenException $e) {
    echo "Error: ".$e->getMessage();
}

echo $token;