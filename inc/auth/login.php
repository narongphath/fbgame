<?php
  $site_root = "http://" . $_SERVER['HTTP_HOST'];
  if (isset($_SESSION['fb_access_token'])) {
    require_once($_SERVER['DOCUMENT_ROOT'] . '/inc/get-user.php');
    $login_link = '<a href="http://facebook-login-mabeautiful.c9users.io/game/get-result.php?gid='. $_GET['gid'] .'" class="waves-effect try-again waves-light btn orange darken-3">សាកល្បង</a>';
  } else{
    $helper = $fb->getRedirectLoginHelper();
    $permissions = ['email']; // Optional permissions
    $loginUrl = $helper->getLoginUrl('https://facebook-login-mabeautiful.c9users.io/inc/auth/fb-callback.php?gid=' . $_GET['gid'] , $permissions);
    $login_link =  '<a href="' . htmlspecialchars($loginUrl) . ' " class="btn waves-effect waves-light blue darken-1 login-fb"><i class="material-icons left">perm_identity</i>ចុះឈ្មោះដើម្បីលេង</a>';
   }