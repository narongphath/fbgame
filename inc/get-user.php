<?php

try {
  // Returns a `Facebook\FacebookResponse` object
$response = $fb->get('/me', $_SESSION['fb_access_token']);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}
$user = $response->getGraphUser();
//var_dump($user);
$image = 'https://graph.facebook.com/'.$user->getId().'/picture?width=400';
$username = $user->getName();
$userid = $user->getId();