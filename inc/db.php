<?php
  try{
     //title$db = new PDO('mysql:host=localhost;dbname=c9;charset=utf8mb4', 'root', '');
     $db = new PDO('mysql:host=107.170.7.132;dbname=app;charset=utf8mb4', 'fbapp', 'fbapp');
     //echo "Connected";
  }catch(PDOException $e){
      echo $e->getMessage();
  }
  
  // check if user's profile was inserted to database 
  function check_exists($sql, $conn){
    $res = $conn->prepare($sql);
    $res->execute();
    $num_row = $res->rowCount();
    return $num_row;
  }
  // display profile data
  function show_single($sql,$conn){
    $res = $conn->query($sql);
    return $res->fetchObject();
  }
  
  function items_list($sql, $conn){
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    return $stmt;
  }

  