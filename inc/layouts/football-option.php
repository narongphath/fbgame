<?php if(isset($_SESSION['fb_access_token'])) : ?>
	<p>លក្ខខណ្ឌក្នុងការជ្រើសរើស</p>
	<form id="football-form" action="<?php echo $site_root . '/game/get-result.php'; ?>" method="get">
		<input type="hidden" name="gid" value="<?php echo $_GET['gid']; ?>" />
		<p>
		<input type="radio" id="all" name="player-scope" checked />
		<label for="all">កីឡាករជាតិ និង កីឡាករអន្តរជាតិ</label>
	  </p>
	  <p>
		<input type="radio" name="player-scope" id="national" value="1" />
		<label for="national">កីឡាករជាតិ</label>
	  </p>
	   <p>
		<input type="radio" name="player-scope" id="international" value="2" />
		<label for="international">កីឡាករអន្តរជាតិ</label>
	  </p>
	  <div class="input-field col s12">
	  <select name="position">
	  	<option value="all" disabled selected>ជ្រើសរើសតួនាទី</option>
			<option value="forward">ខ្សែប្រយុទ្ធ</option>
			<option value="midfielder">ខ្សែបំរើ</option>
			<option value="defender">ខ្សែការពារ</option>
			<option value="goalkeeper">អ្នកចាំទី</option>
	  </select>
	  <label>តើអ្នកមានតួនាទី ជាអ្វីនៅក្នុងក្រុម?</label>
	</div>
	<input type="submit" class="waves-effect try-again waves-light btn orange darken-3" value="សាកល្បង" />
<?php else: ?>
	<?php echo $login_link; ?>
<?php endif; ?>
</form>