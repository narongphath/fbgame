<?php 
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/db.php';
	/* Root directory */
	$dir_root = "http://" . $_SERVER['SERVER_NAME'];
	/* Site root */
	$site_root = "http://" . $_SERVER['HTTP_HOST'];
	/* Secure site root */
	$secure_site_root = "https://" . $_SERVER['HTTP_HOST'];
	/* Current page */
	$page_link = $site_root .  $_SERVER['REQUEST_URI'];
	/* Files include for game page */
	if(isset($_GET['gid'])){
		require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/auth/app-config.php';
		require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/auth/login.php';
		require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/db.php';
		$sql = "SELECT * FROM games WHERE id = " . $_GET['gid'];
		$check_exists = check_exists($sql, $db);
		$page_title = show_single($sql, $db);
		$title = $page_title->name;
	}
	elseif(isset($_GET['pid'])){
		require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/auth/app-config.php';
		require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/auth/login.php';	
		$pid = $_GET['pid'];
	  $gid = substr( $pid, 0, strpos( $pid, '-') );
	  $urlid = substr( $pid, strpos( $pid, '-') + 1);
	  $sql = "SELECT * FROM games WHERE id = " . $gid; 
	  $check_exists = check_exists($sql, $db);
	  $thumb_img = $site_root . '/img/game/'.$gid.'/output/' . $urlid . '.png';
	  $secure_thumb_img = $secure_site_root . '/img/game/'.$gid.'/output/' . $urlid . '.png';
	  $sql = "SELECT * FROM games WHERE id=" . $gid;
	  $game = show_single($sql, $db);
	}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php if(isset($_GET['pid'])) : ?>
    <meta property="og:url"           content="<?php echo $page_link; ?>" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="<?php echo $game->name; ?>" />
    <meta property="og:description"   content="<?php echo $game->description; ?>" />
    <meta property="og:image"         content="<?php echo $thumb_img; ?>" />
    <meta property="og:image:secure_url" content="<?php echo $secure_thumb_img; ?>" /> 
    <meta property="og:image:width" content="600" /> 
    <meta property="og:image:height" content="325" />
  <?php endif; ?>
	<title><?php echo isset($title) ? $title : 'Tos Leng Game'; ?></title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.css" type="text/css" />
	<link rel="stylesheet" href="<?php echo $site_root; ?>/css/app.css" type="text/css" />
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-82776535-1', 'auto');
	  ga('send', 'pageview');
	</script>
</head>
<body>
	<nav class="top-nav">
	  <div class="nav-wrapper">
			<a href="<?php echo $site_root; ?>" class="brand-logo center"><img src="<?php echo $site_root; ?>/img/logo.png"></a>
			<a href="#" data-activates="top-side-nav" class="button-collapse"><i class="material-icons">menu</i></a>
			<ul id="nav-mobile" class="left hide-on-med-and-down">
			  <li><a href="<?php echo $site_root; ?>">ទំព័រដើម</a></li>
			  <li><a href="#">ហ្គេម</a></li>
			  <li><a href="#">អំពីយើង</a></li>
			</ul>
			<?php if( isset($_SESSION['user_id']) ) : ?>
				<ul id="nav-mobile" class="right hide-on-med-and-down">
					<li><a href="<?php echo $site_root; ?>/dashboard/">Dashboard</a></li>
	        <li><a href="<?php echo $site_root . '/dashboard/logout.php'; ?>">Logout</a></li>
	      </ul>
	    <?php else: ?>
	    	<ul id="nav-mobile" class="right hide-on-med-and-down">
	        <li><a href="<?php echo $site_root . '/dashboard/login.php'; ?>">Login</a></li>
	      </ul>
      <?php endif; ?>
	  </div>
	</nav>
	<ul id="top-side-nav" class="side-nav top-nav">
		<li><a href="#">Menu</a></li>
    <li><div class="divider"></div></li>
    <li><a href="<?php echo $site_root; ?>">ទំព័រដើម</a></li>
	  <li><a href="#">ហ្គេម</a></li>
	  <li><a href="#">អំពីយើង</a></li>
  </ul>
	<div class="container">
		<div class="banner-ads">
			<?php //require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/banner-ads.php'; ?>
		</div>
	</div>