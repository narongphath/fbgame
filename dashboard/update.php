<?php 
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/auth/app-config.php'; 
session_start();
if(!isset($_SESSION['user_id']) ){
	header("Location: login.php");
}
	if(isset($_POST['id'])){
		require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/db.php';
		header('content-type:text/html;charset=utf-8');
		$sql = "UPDATE games SET name = :name, title = :title, description = :description WHERE id = " . $_POST['id'];
		$stmt = $db->prepare($sql);
		$stmt->bindValue(':name', $_POST['name']);
		$stmt->bindValue(':title', $_POST['title']);
		$stmt->bindValue(':description', $_POST['description']);
		$stmt->execute();
		header('location: show.php?gid=' . $_POST['id']);
	}
?>
<?php if (isset($_GET['gid'])) : ?>
	<?php require_once 'header.php'; ?>
	
	<div class="dashboard-container">
		<div class="banner-ads">
			<?php //require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/banner-ads.php'; ?>
		</div>
		<div class="game-lists">
			<div class="row">
				<?php 
					$sql = "SELECT * FROM games WHERE id=" . $_GET['gid'];
					$game = show_single($sql, $db);
				?>
				<img src="<?php echo $site_root; ?>/img/game/<?php echo $game->id; ?>/game-<?php echo $game->id; ?>.png" class="response-img">
				<form class="col s12" method="post" action="update.php" >
					<input type="hidden" name="id" value="<?php echo $_GET['gid']; ?>">
		      <div class="row">
		        <div class="input-field col s6">
				      <input value="<?php echo $game->name; ?>" id="name" type="text" name="name">
				      <label class="active" for="first_name2">Game name:</label>
				    </div>
				    <div class="input-field col s6">
				      <input value="<?php echo $game->title; ?>" id="title" type="text" name="title" >
				      <label class="active" for="title">Game title:</label>
				    </div>
		      </div>
		        <div class="row">
		          <div class="input-field col s12">
		            <textarea id="description" class="materialize-textarea" name="description"><?php echo $game->description; ?></textarea>
		            <label for="description">Description</label>
		          </div>
		        </div>
			    <div class="row">
						<button class="btn waves-effect waves-light pink" type="submit" name="action">Update
						<i class="material-icons left">system_update_alt</i>
						</button>
			    </div>
		    </form>
			</div>
		</div>
	</div>
	<?php require_once 'side-nav.php'; ?>
	
	<!-- Include Footer -->
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/footer.php'; ?>
<?php endif; ?>