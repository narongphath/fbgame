<?php
session_start();

if( isset($_SESSION['user_id']) ){
	header("Location: index.php");
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/db.php';

if(!empty($_POST['email']) && !empty($_POST['password'])):
	$records = $db->prepare('SELECT id,email,password FROM users WHERE email = :email');
	$records->bindParam(':email', $_POST['email']);
	$records->execute();
	$results = $records->fetch(PDO::FETCH_ASSOC);

	$message = '';
	if(count($results) > 0 && password_verify($_POST['password'], $results['password']) ){
		$_SESSION['user_id'] = $results['id'];
		header("Location: index.php");

	} else {
		$message = 'Sorry, you have input invalid username or password';
	}

endif;

?>
<?php require_once 'header.php'; ?>

<div class="container login-container">
	<?php if(!empty($message)): ?>
		<div class="card-panel white-text red accent-1"><?php echo $message; ?></div>
	<?php endif; ?>
	<h5>Login</h5>
	<div class="divider"></div>
	<div class="row">
    <form class="col s12" action="login.php" method="POST">
      <div class="row">
				<div class="input-field col s12">
					<i class="material-icons prefix">account_circle</i>
		      <input id="email" type="text" class="validate" name="email">
		      <label for="email">Username:</label>
		    </div>
		  </div>
		  <div class="row">
		    <div class="input-field col s12">
					<i class="material-icons prefix">lock</i>
		      <input id="password" type="password" class="validate" name="password">
		      <label for="password">Password:</label>
		    </div>
		  </div>
	    <div class="row">
	    	<div class="input-field col s12">
	    		<button type="submit" class="waves-effect waves-light btn blue"><i class="material-icons left">lock_open</i>Login to my account</button>
	    	</div>
	    </div>
		</form>
</div>
	
	
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/footer.php'; ?>