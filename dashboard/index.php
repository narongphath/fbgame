<?php 
session_start();
if(!isset($_SESSION['user_id']) ){
	header("Location: login.php");
}
?>	
	
	<?php require_once 'header.php'; ?>
	<div class="container dashboard-container">
		<div class="banner-ads">
			<?php //require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/banner-ads.php'; ?>
		</div>
		<div class="game-lists">
			<div class="row">
				<?php 
					$sql = "SELECT * FROM games";
					$games = items_list($sql, $db);
				?>
				<?php while($game = $games->fetch()) : ?>
					<div class="col s12 m6">
						<p class="post-title">
							<a href="show.php?gid=<?php echo $game['id']; ?>">
								<?php echo $game['title']; ?>
							</a>
						</p>
						<div class="card">
							<div class="card-image">
								<img src="../img/game/<?php echo $game['id']; ?>/game-<?php echo $game['id']; ?>.png" class="response-img">
							</div>
							<div class="card-stacked">
								<div class="card-content">
									<?php
										$excerpt = mb_substr($game['description'], 0, 300) . "...";
									?>
									<p><?php echo $excerpt; ?></p>
								</div>
								<div class="card-action">
									<a href="show.php?gid=<?php echo $game['id']; ?>" class="waves-effect waves-light btn blue">
										<i class="material-icons right">trending_flat</i>
										Read more
									</a>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
	<?php require_once 'side-nav.php'; ?>
	
	<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/footer.php'; ?>