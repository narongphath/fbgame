<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/auth/app-config.php'; ?>
<?php 
session_start();
if(!isset($_SESSION['user_id']) ){
	header("Location: ../login.php");
}
?>
	<?php require_once '../header.php'; ?>
	<div class="dashboard-container">
		<div class="banner-ads">
			<?php //require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/banner-ads.php'; ?>
		</div>
		<?php 
			$sql = "SELECT * FROM players";
			$players = items_list($sql, $db);
		?>
			<!-- Modal Trigger -->
		<table class="bordered">
			<thead>
			  <tr>
		      <th data-field="id">ID</th>
		      <th data-field="name">Player name</th>
		      <th data-field="scope">Player Type</th>
		      <th data-field="position">Player Position</th>
		      <th data-field="action">Action</th>
			  </tr>
			</thead>
			<tbody>
				<?php while($player = $players->fetch()) : ?>
			  <tr>
			    <td><?php echo $player['id']; ?></td>
			    <td><img width="30" class="circle" src="<?php echo $site_root; ?>/img/football-players/<?php echo $player['player_image']; ?>"><?php echo $player['name']; ?></td>
			    <td><?php echo $player['player_scope']; ?></td>
			    <td><?php echo $player['postion']; ?></td>
			    <td>
			    	<a href="update.php?id=<?php echo $player['id'];  ?>" class="btn waves-effect waves-light blue"><i class="material-icons left">mode_edit</i>Edit</a>
			    	<a href="#delete-modal" class="btn waves-effect waves-light red modal-trigger btn-delete" data-href="delete.php?id=<?php echo $player['id'];  ?>"><i class="material-icons left">delete</i>Delete</a>
			    </td>
			  </tr>
			  <?php endwhile; ?>
			  <tr>
			  	<td colspan="4"><a href="new.php" class="btn waves-effect waves-light green"><i class="material-icons left">assignment_ind</i>Add player</a></td>
			  </tr>
			</tbody>
		</table>
	</div>
	<?php require_once '../side-nav.php'; ?>

  <!-- Modal Structure -->
  <div id="delete-modal" class="modal">
    <div class="modal-content">
      <h4>Delete the player</h4>
      <p>Are you sure you want to delete the player?</p>
    </div>
    <div class="modal-footer">
    	<a href="#" class="btn red modal-action waves-effect waves-orange btn-confirm"><i class="material-icons left">delete</i>Delete</a> 
    	
      <a href="#" class=" modal-action modal-close waves-effect waves-orange btn-flat">Cancel</a> 
    </div>
  </div>
	<!-- Include Footer -->
	<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/footer.php'; ?>
	<?php if(isset($_GET['added'])) : ?>
	<script type="text/javascript">
		$(document).ready(function(){
		   Materialize.toast('The player is added', 4000)
		});
	</script>
	<?php endif; ?>