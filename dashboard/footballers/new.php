<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/auth/app-config.php'; ?>
<?php 
session_start();
if(!isset($_SESSION['user_id']) ){
	header("Location: login.php");
}

?>
	<?php require_once '../header.php'; ?>
	<?php 
		if(isset($_POST['name'])){
			if(isset($_FILES['image'])){
				$image = $_FILES['image'];
				$image_name = $image['name'];
				$image_tmp = $image['tmp_name'];
				$image_size = $image['size'];
				$image_error = $image['error'];

				$file_ext = explode('.', $image_name);
				$file_ext = strtolower(end($file_ext));
				$allowed = array('jpg', 'png', 'jpeg');

				if(in_array($file_ext, $allowed)){
					if($image_error === 0){
						if($image_size <= 2048000){
							$image_name_new = uniqid('', true). '.' . $file_ext;
							$file_destination = $_SERVER['DOCUMENT_ROOT'] . '/img/football-players/' . $image_name_new;
							if(move_uploaded_file($_FILES['image']['tmp_name'], $file_destination)){
							}
						}
					}
				}
			}
			$player_name = $_POST['name'];
			$player_scope = $_POST['scope'];
			$player_position = $_POST['position'];
			$stmt = $db->prepare("INSERT INTO players (name, postion, player_scope, player_image) VALUES (:name, :postion, :player_scope, :player_image)");
	    $stmt->bindParam(':name', $player_name);
	    $stmt->bindParam(':postion', $player_position);
	    $stmt->bindParam(':player_scope', $player_scope);
	    $stmt->bindParam(':player_image', $image_name_new);
	    $stmt->execute();
	    header('location: ./?added=true');
		}
	?>
	<div class="dashboard-container">
		<div class="banner-ads">
			<?php //require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/banner-ads.php'; ?>
		</div>
		<div class="row">
		    <form class="col s12" action="" method="post" enctype="multipart/form-data">
			    <div class="row">
	          <div class="input-field col s6">
	            <input id="player_name" type="text" name="name">
	            <label for="player_name">Player Name</label>
	          </div>
	          <div class="file-field input-field col s6">
              <div class="btn">
                <span>Image</span>
                <input type="file" name="image">
              </div>
              <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
              </div>
            </div>
            <div class="input-field col s6">
              <select name="scope">
  				      <option value="1" selected>កីឡាករជាតិ</option>
  				      <option value="2">កីឡាករអន្តរជាតិ</option>
  				    </select>
  				    <label>Player position</label>
            </div>
            <div class="input-field col s6">
              <select name="position">
  				      <option value="forward" selected>Forward</option>
  				      <option value="midfielder">Midfielder</option>
  				      <option value="defender">Defender</option>
  				      <option value="goalkeeper">Goalkeeper</option>
  				    </select>
  				    <label>Player role</label>
            </div>
				    <div class="row">
							<button class="btn waves-effect waves-light green" type="submit" name="action">
							Add Player
							</button>
				    </div>
	        </div>
		    </form>
	  	</div>
	</div>
	<?php //require_once '../side-nav.php'; ?>

<!-- Include Footer -->
<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/footer.php'; ?>