<form class="col s12" action="" method="post" enctype="multipart/form-data">
  <div class="row">
    <div class="input-field col s6">
      <input id="player_name" type="text" name="name">
      <label for="player_name">Player Name</label>
    </div>
    <div class="file-field input-field col s6">
      <div class="btn">
        <span>Image</span>
        <input type="file" name="image">
      </div>
      <div class="file-path-wrapper">
        <input class="file-path validate" type="text">
      </div>
    </div>
    <div class="input-field col s6">
      <select name="scope">
	      <option value="1" selected>កីឡាករជាតិ</option>
	      <option value="2">កីឡាករអន្តរជាតិ</option>
	    </select>
	    <label>Player position</label>
    </div>
    <div class="input-field col s6">
      <select name="position">
	      <option value="forward" selected>Forward</option>
	      <option value="midfielder">Midfielder</option>
	      <option value="defender">Defender</option>
	      <option value="goalkeeper">Goalkeeper</option>
	    </select>
	    <label>Player role</label>
    </div>
    <div class="row">
			<button class="btn waves-effect waves-light green" type="submit" name="action">
			Add Player
			</button>
    </div>
  </div>
</form>