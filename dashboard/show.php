<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/auth/app-config.php'; ?>

<?php 
session_start();
if(!isset($_SESSION['user_id']) ){
	header("Location: login.php");
}
?>

<?php if (isset($_GET['gid'])) : ?>
	<?php require_once 'header.php'; ?>
	
	<div class="dashboard-container container">
		<div class="banner-ads">
			<?php //require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/banner-ads.php'; ?>
		</div>
		<div class="game-lists">
			<div class="row">
				<?php 
					$sql = "SELECT * FROM games WHERE id=" . $_GET['gid'];
					$game = show_single($sql, $db);
				?>
					<div class="col s12 m12">
						<h5><?php echo $game->name; ?></h5>
						<div class="card">
							<div class="card-image">
								<img src="../img/game/<?php echo $game->id; ?>/game-<?php echo $game->id; ?>.png" class="response-img">
							</div>
							<div class="card-stacked">
								<div class="card-content">
									<p><?php echo $game->description; ?></p>
								</div>
								<div class="card-action">
									<a href="update.php?gid=<?php echo $game->id; ?>" class="waves-effect waves-light btn green">
										<i class="material-icons left">mode_edit</i>
										EDIT
									</a>
									<?php if($_GET['gid'] == 2) : ?>
										<a href="./footballers/" class="waves-effect waves-light btn blue">
											<i class="material-icons left">list</i>
											See all players
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
	<?php require_once 'side-nav.php'; ?>
<?php endif; ?>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/footer.php'; ?>