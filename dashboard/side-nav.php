<?php 
	$dashboard_url = $site_root . "/dashboard/";
	$sql = "SELECT id, title from games";
	$game_titles = items_list($sql, $db);
?>
<aside id="slide-out" class="side-nav fixed">
	<ul class="game-menu">
	<li><a href="<?php echo $dashboard_url; ?>">Dashboard</a></li>
		<?php while($title = $game_titles->fetch()) : ?>
			<li><a href="<?php echo $dashboard_url; ?>show.php?gid=<?php echo $title['id']; ?>"><?php echo $title['title']; ?></a></li>
		<?php endwhile; ?>
	</ul>
</aside>