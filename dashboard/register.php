<?php

session_start();

if( isset($_SESSION['user_id']) ){
	header("Location: index.php");
}
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/db.php';

$message = '';
if(!empty($_POST['email']) && !empty($_POST['password'])){
	// Enter the new user in the database
	$sql = "INSERT INTO users (email, password) VALUES (:email, :password)";
	$stmt = $db->prepare($sql);
	$stmt->bindParam(':email', $_POST['email']);
	$stmt->bindParam(':password', password_hash($_POST['password'], PASSWORD_BCRYPT));

	if( $stmt->execute()){
		$message = 'Successfully created new user';
	}else{
		$message = 'Sorry there must have been an issue creating your account';
	}
}

?>

<?php require_once 'header.php'; ?>

<div class="container login-container">
	<?php if(!empty($message)): ?>
		<div class="card-panel white-text red accent-1"><?php echo $message; ?></div>
	<?php endif; ?>
	<h5>Register</h5>
	<div class="divider"></div>
	<div class="row">
    <form class="col s12" action="register.php" method="POST">
    	<div class="row">
    		<div class="input-field col s12">
					<i class="material-icons prefix">account_circle</i>
		      <input id="email" type="text" class="validate" name="email">
		      <label for="email">Email Address:</label>
		    </div>
    		<div class="input-field col s12">
					<i class="material-icons prefix">lock</i>
		      <input id="password" type="password" class="validate" name="password">
		      <label for="email">Password:</label>
		    </div>
    		<div class="input-field col s12">
					<i class="material-icons prefix">lock</i>
		      <input id="confirm_password" type="password" class="validate" name="confirm_password">
		      <label for="confirm_password">Confirm password:</label>
		    </div>
    	</div>
			<button type="submit" class="waves-effect waves-light btn green"><i class="material-icons left">lock_open</i>Create my account</button>

		</form>
	</div>
</div>


<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/layouts/footer.php'; ?>