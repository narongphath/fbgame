$(document).ready(function(){
    $('.modal-trigger').leanModal();
	$('.fb-share-btn').on('click', function(e){
		window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
		return false;
	});
	$('select').material_select();
	
	$('.login-fb').click(function(e){
		$('#football-form').submit();
	});

	$(".button-collapse").sideNav();
	
	//confirm delete
	$('.btn-delete').click(function(e){
		$deleteUrl = $(this).data('href');
		$('.btn-confirm').attr('href', $deleteUrl);
	});
});