<?php
require 'vendor/autoload.php';
use Intervention\Image\ImageManagerStatic as Image;
Image::configure(array('driver' => 'imagick'));
//Blacklisted game
function blacklisted($img,$username, $userid, $urlid){
    // Random answer
    $random_answers = array(0, 1, 2);
    $random_number = array_rand($random_answers, 1);
    $answer = $random_answers[$random_number];
    //Set profile size
    $profile = Image::make($img)->resize(100,100);
    $profile_img = '../img/profiles/'. $userid .'.png';
    $profile->save($profile_img);
    $image = Image::make('../img/game/1/sample-1.jpg');
    $image->text($username, 
        300, 190, function($font) {
        $font->file('../fonts/OpenSans-ExtraBold.ttf');
        $font->size(16);
        $font->color('#fff');
        $font->align('center');
        $font->valign('top');
    });
    $image->insert('../img/answer/answer_' . $answer .'.png', 'bottom-center');
    $output_img = '../img/game/1/output/'. $urlid .'.png';
    $image->insert($profile_img, 'bottom-left', 251, 150)->save($output_img);
    return '<img src="../'. $output_img.'" class="responsive-img z-depth-3">';
}
function footballer($img,$username, $userid, $urlid, $player_image, $player_name){
    $profile = Image::make($img)->resize(150,150);
    $profile_img = '../img/profiles/'. $userid .'.png';
    $profile->save($profile_img);
    $player_img = Image::make('../img/football-players/'. $player_image);
    $image = Image::make('../img/game/2/sample-2.png');
    $image->text($username, 
        130, 270, function($font) {
        $font->file('../fonts/OpenSans-ExtraBold.ttf');
        $font->size(16);
        $font->color('#fff');
        $font->align('center');
        $font->valign('top');
    });
    $image->text($player_name, 
        470, 270, function($font) {
        $font->file('../fonts/OpenSans-ExtraBold.ttf');
        $font->size(16);
        $font->color('#fff');
        $font->align('center');
        $font->valign('top');
    });
    //$image->insert('../img/answer/answer_' . $answer .'.png', 'bottom-center');
    $output_img = '../img/game/2/output/'. $urlid .'.png';
    $image->insert($player_img, 'bottom-right', 60, 75);
    $image->insert($profile_img, 'bottom-left', 56, 75)->save($output_img);
    return '<img src="../'. $profile_img.'" class="responsive-img z-depth-3">';
}
function traintobusan($img, $userid, $urlid, $char_img, $char_text){
    $profile = Image::make($img)->resize(145,145);
    $profile_img = '../img/profiles/'. $userid .'.png';
    $profile->save($profile_img);
    
    $image = Image::make('../img/game/3/sample-3.png');
    //$image->insert('../img/answer/answer_' . $answer .'.png', 'bottom-center');
    $output_img = '../img/game/3/output/'. $urlid .'.png';
    $image->insert('../img/game/3/answer/'. $char_img, 'bottom-right', 70, 164);
    $image->insert('../img/game/3/answer/'. $char_text, 'bottom-right', 0, 110);
    $image->insert($profile_img, 'bottom-left', 63, 164)->save($output_img);
    return '<img src="../'. $profile_img.'" class="responsive-img z-depth-3">';
}
